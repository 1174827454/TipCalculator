
// TipCalculatorDlg.h : 头文件
//

#pragma once
#include "MyEdit.h"
#include "afxcmn.h"
#include "afxwin.h"

// CTipCalculatorDlg 对话框
class CTipCalculatorDlg : public CDialogEx
{
// 构造
public:
	CTipCalculatorDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_TIPCALCULATOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnStnClickedBillamount();
	
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CMyEdit BillTotal;


	afx_msg void OnClickedSubmit();
	CSliderCtrl SatisfactionSlider;
	CEdit TipRate;

	CEdit BillPay;

	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	CEdit amount;
};
