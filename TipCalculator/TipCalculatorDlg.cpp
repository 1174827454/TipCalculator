
// TipCalculatorDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "TipCalculator.h"
#include "TipCalculatorDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int m_int;
int result;

// CTipCalculatorDlg 对话框



CTipCalculatorDlg::CTipCalculatorDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTipCalculatorDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTipCalculatorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BILLTOTAL, BillTotal);
	//  DDX_Text(pDX, IDC_TIPRATE, TipRate);
	//  DDV_MaxChars(pDX, TipRate, 10);
	DDX_Control(pDX, IDC_UserSatisfaction, SatisfactionSlider);
	DDX_Control(pDX, IDC_TIPRATE, TipRate);
	//  DDX_Control(pDX, IDC_BILLPAY, BillPay);
	DDX_Control(pDX, IDC_BILLPAY, BillPay);
	DDX_Control(pDX, IDC_AMOUNT, amount);
}

BEGIN_MESSAGE_MAP(CTipCalculatorDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()

ON_BN_CLICKED(IDC_SUBMIT, &CTipCalculatorDlg::OnClickedSubmit)
//ON_EN_CHANGE(IDC_TIPRATE, &CTipCalculatorDlg::OnEnChangeTiprate)
ON_WM_HSCROLL()
END_MESSAGE_MAP()


// CTipCalculatorDlg 消息处理程序

BOOL CTipCalculatorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CString str;
	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标
	m_int=0;
	result=5;
	SatisfactionSlider.SetRange(0,100);
	SatisfactionSlider.SetTicFreq(1);
	str.Format(_T("%d"),result);
	TipRate.SetWindowTextW(str);
	::SetWindowPos(this->m_hWnd,HWND_BOTTOM,0,0,660,480,SWP_NOZORDER|SWP_NOMOVE);

	
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CTipCalculatorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CTipCalculatorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



BOOL CTipCalculatorDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if(pMsg->wParam==VK_ESCAPE){
	return TRUE;
	}
	if(pMsg->wParam==VK_RETURN){
		NextDlgCtrl();
		return TRUE;
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CTipCalculatorDlg::OnClickedSubmit()
{
	// TODO: 在此添加控件通知处理程序代码
	

	CString str1,str3,str4;
	BillTotal.GetWindowTextW(str1);
	if(str1==""){
	MessageBox(_T("请输入金额！"),_T("警告"),MB_OK);
	BillTotal.SetWindowTextW(_T(""));
	amount.SetWindowTextW(_T(""));
	BillPay.SetWindowTextW(_T(""));
	}else if(str1.GetLength()>16){
	MessageBox(_T("你输入的数字长度超出范围！"),_T("警告"),MB_OK);
	BillPay.SetWindowTextW(_T(""));
	BillTotal.SetWindowTextW(_T(""));
	}
	else{
		
	double m=_tstof(str1)*result/100;
	double n=m+_tstof(str1);
	str3.Format(_T("%lf"),m);
	str4.Format(_T("%lf"),n);
	BillPay.SetWindowTextW(str3);
	amount.SetWindowTextW(str4);
	}
	
}



void CTipCalculatorDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CString str2;
	m_int=SatisfactionSlider.GetPos();

	result=5+m_int*15/100;
	str2.Format(_T("%d"),result);
	
	TipRate.SetWindowTextW(str2);
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}
