// MyEdit.cpp : 实现文件
//

#include "stdafx.h"
#include "TipCalculator.h"
#include "MyEdit.h"


// CMyEdit

IMPLEMENT_DYNAMIC(CMyEdit, CEdit)

CMyEdit::CMyEdit()
{

}

CMyEdit::~CMyEdit()
{
}


BEGIN_MESSAGE_MAP(CMyEdit, CEdit)
END_MESSAGE_MAP()



// CMyEdit 消息处理程序




BOOL CMyEdit::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	CString str;
	int nPos=0;
	
	if(pMsg->message==WM_CHAR)
	{
		if(pMsg->wParam=='.'){
		GetWindowText(str);
		nPos=str.GetLength();
		nPos=str.Find('.');
		if(nPos>=0){
		MessageBox(_T("你已经输入了小数点啦！"),_T("警告"),MB_OK);
		return TRUE;
		}
		
		}

		if(pMsg->wParam!='.'&&(pMsg->wParam>'9'||pMsg->wParam<'0')&&pMsg->wParam!='\b'){
		MessageBox(_T("你输入了非数字！"),_T("警告"),MB_OK);
		return TRUE;
		}
			

	}
	return CEdit::PreTranslateMessage(pMsg);
}
