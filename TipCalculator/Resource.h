//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 TipCalculator.rc 使用
//
#define IDD_TIPCALCULATOR_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDC_BILLAMOUNT                  1000
#define IDC_BILLTOTAL                   1001
#define IDC_TIPRATE                     1002
#define IDC_UserSatisfaction            1003
#define IDC_SUBMIT                      1006
#define IDC_BILLPAY                     1007
#define IDC_USERPAY                     1008
#define IDC_STATIC_TOTAL                1010
#define IDC_EDIT1                       1011
#define IDC_AMOUNT                      1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
